class ApplicationController < ActionController::Base
  before_action :verify_authorized, unless: :devise_controller?
  # before_action :configure_permitted_parameters, if: :devise_controller?

  private

  def verify_authorized
    redirect_to new_user_session_path unless user_signed_in?
  end

  # def configure_permitted_parameters
  #   devise_parameter_sanitizer.permit(:sign_up) do |params|
  #     params.permit(:email, :password, :password_confirmation)
  #   end
  #
  #   devise_parameter_sanitizer.permit(:account_update) do |params|
  #     params.permit(:email, :password, :password_confirmation, :current_password)
  #   end
  # end
end
