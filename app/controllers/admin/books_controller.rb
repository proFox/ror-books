class Admin::BooksController < Admin::AdminController
  before_action :find_authors, only: %i[new create edit update]
  before_action :find_book, only: %i[show edit update destroy]

  def index
    @books = Book.all.page(params[:page])
  end

  def show; end

  def new
    @book = Book.new
  end

  def edit; end

  def create
    @book = Book.new(book_params)

    if @book.save
      flash[:success] = t('admin.books.new.notifications.successfully_created')
      redirect_to admin_books_path
    else
      flash.now[:danger] = t('admin.books.new.notifications.not_created')
      render :new
    end
  end

  def update
    if @book.update(book_params)
      flash[:success] = t('admin.books.edit.notifications.successfully_updated')
      redirect_to admin_books_path
    else
      flash.now[:danger] = t('admin.books.edit.notifications.not_updated')
      render :edit
    end
  end

  def destroy
    if @book.destroy
      flash[:success] = t('admin.books.destroy.notifications.successfully_deleted')
    else
      flash[:danger] = t('admin.books.destroy.notifications.not_deleted')
    end
    redirect_to admin_books_path
  end

  private

  def find_book
    @book = Book.find(params[:id])
  end

  def find_authors
    @authors = Author.all
  end

  def book_params
    params.require(:book).permit(:title, :cover, author_ids: [])
  end
end
