class Admin::AuthorsController < Admin::AdminController
  before_action :find_author, only: %i[show edit update destroy]

  def index
    @authors = Author.all.page(params[:page])
  end

  def show; end

  def new
    @author = Author.new
  end

  def edit; end

  def create
    @author = Author.new(author_params)

    if @author.save
      flash[:success] = t('admin.authors.new.notifications.successfully_created')
      redirect_to admin_authors_path
    else
      flash.now[:danger] = t('admin.authors.new.notifications.not_created')
      render :new
    end
  end

  def update
    if @author.update(author_params)
      flash[:success] = t('admin.authors.edit.notifications.successfully_updated')
      redirect_to admin_authors_path
    else
      flash.now[:danger] = t('admin.authors.edit.notifications.not_updated')
      render :edit
    end
  end

  def destroy
    if @author.destroy
      flash[:success] = t('admin.authors.destroy.notifications.successfully_deleted')
    else
      flash[:danger] = t('admin.authors.destroy.notifications.not_deleted')
    end
    redirect_to admin_authors_path
  end

  private

  def find_author
    @author = Author.find(params[:id])
  end

  def author_params
    params.require(:author).permit(:name)
  end
end
