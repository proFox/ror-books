class Admin::AdminController < ApplicationController
  layout 'admin/main'

  before_action :admin?

  private

  def admin?
    redirect_to root_path unless user_signed_in? || current_user.is_admin?
  end
end
