class Public::PublicController < ApplicationController
  layout 'public/main'

  before_action :admin?, unless: :devise_controller?

  private

  def admin?
    redirect_to admin_root_path if user_signed_in? && current_user.is_admin?
  end
end
