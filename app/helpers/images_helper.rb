module ImagesHelper
  def picture(image)
    if image.attached?
      url_for(image)
    else
      image_path('no-photo.png')
    end
  end
end
