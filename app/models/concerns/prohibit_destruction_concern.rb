module ProhibitDestructionConcern
  extend ActiveSupport::Concern

  included do
    before_destroy :prohibit_destruction

    private

    def prohibit_destruction
      errors[:base] << 'Protection against accidental deletion'
      throw(:abort)
    end
  end
end
