# == Schema Information
#
# Table name: books
#
#  id         :bigint(8)        not null, primary key
#  cover      :string
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Book < ApplicationRecord
  has_many :books_and_authors
  has_many :authors, through: :books_and_authors, dependent: :destroy

  has_one_attached :cover

  validates :title,
            presence: true,
            length: { minimum: 5, maximum: 300 }

  default_scope { order(title: :asc) }
end
