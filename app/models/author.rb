# == Schema Information
#
# Table name: authors
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Author < ApplicationRecord
  has_many :books_and_authors
  has_many :books, through: :books_and_authors, dependent: :destroy

  validates :name,
            presence: true,
            length: { minimum: 5, maximum: 100 }

  default_scope { order(name: :asc) }
end
