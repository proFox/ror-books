# == Route Map
#
#                    Prefix Verb   URI Pattern                                                                              Controller#Action
#                      root GET    /                                                                                        public/home#index
#          new_user_session GET    /users/sign_in(.:format)                                                                 devise/sessions#new
#              user_session POST   /users/sign_in(.:format)                                                                 devise/sessions#create
#      destroy_user_session DELETE /users/sign_out(.:format)                                                                devise/sessions#destroy
#         new_user_password GET    /users/password/new(.:format)                                                            devise/passwords#new
#        edit_user_password GET    /users/password/edit(.:format)                                                           devise/passwords#edit
#             user_password PATCH  /users/password(.:format)                                                                devise/passwords#update
#                           PUT    /users/password(.:format)                                                                devise/passwords#update
#                           POST   /users/password(.:format)                                                                devise/passwords#create
#                admin_root GET    /admin(.:format)                                                                         admin/books#index
#               admin_books GET    /admin/books(.:format)                                                                   admin/books#index
#                           POST   /admin/books(.:format)                                                                   admin/books#create
#            new_admin_book GET    /admin/books/new(.:format)                                                               admin/books#new
#           edit_admin_book GET    /admin/books/:id/edit(.:format)                                                          admin/books#edit
#                admin_book PATCH  /admin/books/:id(.:format)                                                               admin/books#update
#                           PUT    /admin/books/:id(.:format)                                                               admin/books#update
#                           DELETE /admin/books/:id(.:format)                                                               admin/books#destroy
#             admin_authors GET    /admin/authors(.:format)                                                                 admin/authors#index
#                           POST   /admin/authors(.:format)                                                                 admin/authors#create
#          new_admin_author GET    /admin/authors/new(.:format)                                                             admin/authors#new
#         edit_admin_author GET    /admin/authors/:id/edit(.:format)                                                        admin/authors#edit
#              admin_author PATCH  /admin/authors/:id(.:format)                                                             admin/authors#update
#                           PUT    /admin/authors/:id(.:format)                                                             admin/authors#update
#                           DELETE /admin/authors/:id(.:format)                                                             admin/authors#destroy
#        rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
# rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#        rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
# update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#      rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  root 'public/home#index'

  devise_for :users

  namespace :admin do
    root 'books#index'

    resources :books, except: %i[show]
    resources :authors, except: %i[show]
  end
end
