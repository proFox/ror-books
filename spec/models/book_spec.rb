require 'rails_helper'

RSpec.describe Book, type: :model do
  context 'creature' do
    it 'with a normal title' do
      Book.create(title: 'God title')
      expect(Book.count).to be >= 1
    end

    it 'with a short title' do
      book = Book.new(title: 'TD')
      book.valid?
      expect(book.errors[:title].size).to eq(1)
    end

    it 'with a long title' do
      book = Book.new(title: 'TD' * 400)
      book.valid?
      expect(book.errors[:title].size).to eq(1)
    end

    it 'with the addition of two new books' do
      book = Book.create(title: 'God title')
      author1 = Author.create(name: 'Author 1')
      author2 = Author.create(name: 'Author 2')

      book.authors << author1
      book.authors << author2

      expect(book.authors.count).to eq(2)
    end
  end
end
