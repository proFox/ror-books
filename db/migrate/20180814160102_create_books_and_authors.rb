class CreateBooksAndAuthors < ActiveRecord::Migration[5.2]
  def change
    create_table :books_and_authors do |t|
      t.references :book, index: true, foreign_key: true
      t.references :author, index: true, foreign_key: true

      t.timestamps
    end
  end
end
